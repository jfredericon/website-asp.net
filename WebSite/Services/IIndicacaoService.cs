﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Models;

namespace WebSite.Services
{
    public interface IIndicacaoService
    {

        JObject CreateIndicacao(Indicacao indicacao);
    }
}
