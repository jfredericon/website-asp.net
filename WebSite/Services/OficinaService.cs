﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Models;
using Newtonsoft.Json.Linq;

namespace WebSite.Services
{
    public class OficinaService: IOficinaService
    {
        private static List<Oficina> oficinas = new List<Oficina>();
        private string baseUri = "http://app.hinovamobile.com.br/ProvaConhecimentoWebApi/Api/Oficina?codigoAssociacao=601&cpfAssociado=";

        public  List<Oficina> GetAll()
        {
            var client = HttpClientIntance.GetHttpClientInstance();

            client.BaseAddress = new Uri(baseUri);
            client.DefaultRequestHeaders.Accept.Clear();

            var response = client.GetAsync(baseUri).Result;
            var retorno = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            JArray oficinasJSON = (JArray)retorno["ListaOficinas"];

            oficinas = new List<Oficina>();

            foreach (var item in oficinasJSON)
            {
                oficinas.Add(new Oficina()
                {
                    Id = (int)item["Id"],
                    Nome = item["Nome"].ToString(),
                    Descricao = item["Descricao"].Equals(null) ? String.Empty : item["Descricao"].ToString(),
                    DescricaoCurta = item["DescricaoCurta"].Equals(null) ? String.Empty : item["DescricaoCurta"].ToString(),
                    Endereco = item["Endereco"].Equals(null) ? String.Empty : item["Endereco"].ToString(),
                    Latitude = item["Latitude"].Equals(null) ? String.Empty : item["Latitude"].ToString(),
                    Longitude = item["Longitude"].Equals(null) ? String.Empty : item["Longitude"].ToString(),
                    Foto = item["Foto"].Equals(null) ? String.Empty : "data:image/jpeg;base64," + item["Foto"].ToString(),
                    AvaliacaoUsuario = (int)item["AvaliacaoUsuario"],
                    CodigoAssociacao = (int)item["CodigoAssociacao"],
                    Email = item["Email"].Equals(null) ? String.Empty : item["Email"].ToString(),
                    Telefone1 = item["Telefone1"].Equals(null) ? String.Empty : item["Telefone1"].ToString(),
                    Telefone2 = item["Telefone2"].Equals(null) ? String.Empty : item["Telefone2"].ToString(),
                    Ativo = (bool)item["Ativo"]
                });
            }

            return oficinas;
        }

        public Oficina GetOne(int id)
        {
            var oficina = oficinas.Find( x => x.Id == id);
            
            return oficina;
        }
    }

}
