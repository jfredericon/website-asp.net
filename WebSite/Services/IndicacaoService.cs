﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace WebSite.Services
{
    public class IndicacaoService : IIndicacaoService
    {
        private string baseUri = "http://app.hinovamobile.com.br/ProvaConhecimentoWebApi/Api/Indicacao";

        public JObject CreateIndicacao(Indicacao indicacao)
        {
            var client = HttpClientIntance.GetHttpClientInstance();

            client.BaseAddress = new Uri(baseUri);
            client.DefaultRequestHeaders.Accept.Clear();
            var now = DateTime.Now;
            indicacao.DataCriacao = now;


            var json = JsonConvert.SerializeObject(new {
                Indicacao = indicacao,
                Remetente = indicacao.Remetente,
                Copias = indicacao.Copias
            }
                );

            var buffer = System.Text.Encoding.UTF8.GetBytes(json);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = client.PostAsync(baseUri, byteContent ).Result;
            var retorno = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            return retorno;
        }
    }
}
