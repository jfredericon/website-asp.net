﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Models;

namespace WebSite.Services
{
    public interface IOficinaService
    {
        List<Oficina> GetAll();
        Oficina GetOne(int id);
    }
}
