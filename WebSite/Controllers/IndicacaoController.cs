﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebSite.Models;
using WebSite.Services;

namespace WebSite.Controllers
{
    public class IndicacaoController : Controller
    {

        private IIndicacaoService IndicacaoService { get; }

        public IndicacaoController(IIndicacaoService indicacaoService)
        {
            IndicacaoService = indicacaoService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(Indicacao indicacao)
        {

            var retorno = IndicacaoService.CreateIndicacao(indicacao);

            if (retorno["Sucesso"].ToString() == "")
            {
                ViewData["Error"] = retorno["RetornoErro"]["retornoErro"].ToString();
            }
            else
            {
                ViewData["Sucesso"] = retorno["Sucesso"].ToString();
            }

            ModelState.Clear();
            return View();
        }
    }
}