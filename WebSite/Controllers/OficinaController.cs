﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebSite.Models;
using WebSite.Services;

namespace WebSite.Controllers
{
    public class OficinaController : Controller
    {

        private IOficinaService OficinaService { get; }

        public OficinaController(IOficinaService oficinaService)
        {
            OficinaService = oficinaService;
        }

        public IActionResult Index()
        {
            List<Oficina> oficinas = OficinaService.GetAll();

            return View(oficinas);
        }

        public IActionResult Detalhe(int id)
        {

            var oficina = OficinaService.GetOne(id);

            return View(oficina);
        }
    }
}