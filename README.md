# WebSite em  Asp.Net

O projeto consiste em um web site com as seguintes páginas:

### Consultar Oficinas:  
Tela que exibe uma lista de oficinas com informações bem sucintas como nome e uma breve descrição,  
onde ao clicar no botão detalhes o usuário é direcionado para a tela de detalhe da oficina.

### Detalhe da Oficina:  
Tela que exibe as informações detalhadas da oficina como nome, descrição dos serviços,  
endereço, email, telefone se houver cadastrado, etc.

### Indicação de Amigo:  
Tela que exibe um fomulário para que o usuário preencha e envie a indicação de um amigo.